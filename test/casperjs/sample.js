/* global casper */

'use strict';
casper.start('http://casperjs.org/', function() {
    this.echo(this.getTitle());
});


casper.thenOpen('http://127.0.0.1:9000/#', function() {
    this.echo(this.getTitle());
});


casper.run();